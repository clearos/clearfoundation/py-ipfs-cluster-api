import pytest
from ipfscluster import PinOptions, AddOptions

def test_id(client):
    """Tests the `/id` endpoint of the cluster.
    """
    r = client.id()
    assert "id" in r
    assert "addresses" in r
    assert r["error"] == ''

def test_addbytes(client):
    """Tests adding bytes to the cluster using context manager.
    """
    m = {'name': 'Mary-bytes',
         'cid': {'/': 'QmZfF6C9j4VtoCsTp4KSrhYH47QMd3DNXVZBKaxJdhaPab'},
         'size': 30}
    with client:
        r = client.add_bytes(b"Mary had a little lamb", name="Mary-bytes")
    assert r == m

def test_addfile(client):
    """Tests adding bytes to the cluster using context manager.
    """
    pinopts = PinOptions(name="testdata-file")
    addopts = AddOptions(pinopts=pinopts, shard=False, hashfun="sha2-256")
    m = {'name': 'testdata.txt',
         'cid': {'/': 'QmRWV9tw9pZdUMovDXjz2jsJcH7UqEZCMRguadYpDqBJmQ'},
         'size': 31}
    with client:
        r = client.add_files("tests/testdata.txt", addopts=addopts)
    assert r == m

    r2 = client.allocations.ls("QmRWV9tw9pZdUMovDXjz2jsJcH7UqEZCMRguadYpDqBJmQ")
    assert r2["name"] == "testdata-file"

def test_add(client):
    """Tests adding text to the cluster.
    """
    m = {'name': 'Mary-string',
          'cid': {'/': 'QmZfF6C9j4VtoCsTp4KSrhYH47QMd3DNXVZBKaxJdhaPab'},
          'size': 30}
    r = client.add_str(u"Mary had a little lamb", name="Mary-string")
    assert r == m

def test_generator_text(client):
    """Tests adding text from a generator.
    """
    text = ["This is a", "very long string", "of text."]
    def gen_text():
        for line in text:
            yield line

    m = {'name': 'text-generator',
         'cid': {'/': 'QmTSqwyv6niF9nwgpqRhHDVdN24XfkcnqxCUhvN43bjsme'},
         'size': 41 }
    r = client.add_str(gen_text(), name="text-generator")
    assert r == m

    r2 = client.allocations.ls("QmTSqwyv6niF9nwgpqRhHDVdN24XfkcnqxCUhvN43bjsme")
    assert r2["name"] == 'text-generator'

def test_addjson(client):
    """Tests adding JSON to the cluster."""
    m = {'name': 'JSON-string',
          'cid': {'/': 'QmSsf2JAfrKaVPgPpFY1qEjLTiiqaWt8aHFRRKrx3dRXBz'},
          'size': 35}
    r = client.add_json({'one': 1, 'two': 2, 'three': 3}, name="JSON-string")
    assert r == m
