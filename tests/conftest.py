import pytest
from ipfscluster import connect

@pytest.fixture(scope="session")
def client(request):
    """Returns a value indicating if the OS is Windows or not.
    """
    result = connect()
    #Add some simple data to the cluster so that the other unit tests have
    #data and pins to work with. These methods get tested again in the
    #test_client.py file.
    result.add_str(u"Mary had a little lamb")
    result.add_json({'one': 1, 'two': 2, 'three': 3})
    return result
